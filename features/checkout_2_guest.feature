Feature: Zákazník v pořádku zkontroloval obsah košíku a pokračuje v objednávání jako host.
	Protože nepočítá s tím, že by se zde vracel, tak provede jednorázový
	nákup bez registrace.
	(Objednávání - 2. krok (osobní údaje, adresa) - návštěvník bez registrace, jednorázový nákup)

	Scenario Outline: Zákazník vyplnil formulář, ale přehlédl povinný údaj.
		Given zakaznik se nachazi na osobnich udajich a adrese
		When zakaznik nevyplni povinny udaj <required>
		Then zakaznik je upozornen
		
	 Examples: Povinne udaje
   	  | required                   |  
	  | firstname                  |
      | lastname                   |
      | email                      |
	  | telephone                  |
      | address_1                  |
      | city                       |
      | postcode                   |
	  | country_id                 |
	  | zone_id	                   |


	Scenario Outline: Zákazník vyplnil všechny povinné údaje ve formuláři,
		ale u některého pole omylem nenapsal všechny znaky.
		Given zakaznik se nachazi na osobnich udajich a adrese
		When zakaznik vyplni min jak <min> pro <required>
		Then zakaznik je upozornen
		
	 Examples: Povinne udaje - min
   	  | required                   | min    |
	  | firstname                  | 1      |
      | lastname                   | 1      |
	  | telephone                  | 3      |
      | address_1                  | 3      |
      | city                       | 2      |
      | postcode                   | 2      |

	Scenario Outline: Zákazník vyplnil všechny povinné prvky formuláře,
		ale u některého pole se nedopatřením přepsal.
		Given zakaznik se nachazi na osobnich udajich a adrese
		When zakaznik vyplni vice jak <max> pro <required>
		Then zakaznik je upozornen
		
	 Examples: Povinne udaje - max
   	  | required                   | max    |
	  | firstname                  | 32     |
      | lastname                   | 32     |
	  | telephone                  | 32     |
      | address_1                  | 128    |
      | city                       | 128    |
      | postcode                   | 10     |

	Scenario: Zákazník při vyplňování formuláře špatně vyplnil pole s e-mailem,
		čímž vzikla nevalidní adresa.
		Given zakaznik se nachazi na osobnich udajich a adrese
		When zakaznik zada spatny format emailu
		Then zakaznik je upozornen na jeho validitu

	Scenario: Zákazník vyplnil všechny pole formuláře v pořádku a chce
		pokračovat na další krok objednávání.
		Given zakaznik se nachazi na osobnich udajich a adrese
		When zakaznik zada spravne udaje
		Then zakaznik je pusten na vyber platby

