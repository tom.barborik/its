Feature: Zákazník vpořádku zkontroloval obsah košíku a protože je zde
	registrovaný, tak by se chtěl na prvním kroku přihlásit. 
	(Objednávání - 1. krok (nastavení typu zákazníka))

	Scenario Outline: Zákazník se pokouší přihlásit na svůj účet.
		Given zakaznik ma v kosiku 44
		When zakaznik se pokusi prihlasit s <username> <password>
		Then zakaznik byl <state>

 		Examples: Shoppers
   		| username                   | password    | state       |
   		| barborik.tom25@gmail.com   | heslo1234   | prihlasen   |
		| barborik.tom25@gmail.com   | heslo       | neprihlasen |
