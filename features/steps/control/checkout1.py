from .control import *;

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Checkout1Control (Control):
    def user_login(self, username, password):
        self.go_to("index.php?route=checkout/checkout")
        self.logged = False

        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "input#input-email")))

        name = self.driver.find_element_by_id("input-email")
        name.clear()
        name.send_keys(str(username))

        passw = self.driver.find_element_by_id("input-password")
        passw.clear()
        passw.send_keys(str(password))

        button = self.driver.find_element_by_id("button-login")
        button.click()

        try:
            WebDriverWait(self.driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "#content .alert")))
        except Exception:
            self.logged = True

        return self.logged

    def as_guest(self):
        self.go_to("index.php?route=checkout/checkout")
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "input[value='guest'][type='radio'][name='account']")))
        radio = self.driver.find_element_by_css_selector("input[value='guest'][type='radio'][name='account']")
        radio.click()

        button = self.driver.find_element_by_id("button-account")
        button.click()

    def as_to_register(self):
        self.go_to("index.php?route=checkout/checkout")
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "input[value='register'][type='radio'][name='account']")))
        radio = self.driver.find_element_by_css_selector("input[value='register'][type='radio'][name='account']")
        radio.click()

        button = self.driver.find_element_by_id("button-account")
        button.click()
    
    def user_logged(self):
        return self.logged
