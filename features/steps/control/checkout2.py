from .control import *;

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Checkout2Control (Control):
    def fill_billing_delivery(self, data, goto = None):
        if goto is not None:
            self.go_to(goto)
        
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "#collapse-payment-address")))
        panel = self.driver.find_element_by_id("collapse-payment-address")
        inputs = panel.find_elements_by_css_selector("input")
        for input in inputs:
            if input.get_attribute("name") in data:
                input.clear()
                input.send_keys(data[input.get_attribute("name")])

        selects = panel.find_elements_by_css_selector("select")
        for select in selects:
            if select.get_attribute("name") in data:
                select.find_element_by_xpath("//*[text() = '"+data[select.get_attribute("name")]+"']").click()

        panel.find_element_by_id("button-guest").click()
        try:
            WebDriverWait(self.driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "#collapse-payment-address .has-error")))
            return False
        except Exception:
            return True

        return True

        

